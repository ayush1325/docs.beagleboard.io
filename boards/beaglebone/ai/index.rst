.. _bbai-home:

BeagleBone AI
###############

BeagleBone AI is based on the Texas Instruments `AM5729 <https://www.ti.com/product/AM5729>`_ dual-core 
Cortex-A15 SoC with flexible BeagleBone Black header and mechanical compatibility. BeagleBone AI makes 
it easy to explore how artificial intelligence (AI) can be used in everyday life via the TI C66x 
digital-signal-processor (DSP) cores and embedded-vision-engine (EVE) cores supported through an 
optimized TIDL machine learning OpenCL API with pre-installed tools. Focused on everyday 
automation in industrial, commercial and home applications.

.. admonition:: License Terms

    * This documentation is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`__
    * Design materials and license can be found in the `git repository <https://git.beagleboard.org/beagleboard/beaglebone-ai>`__
    * Use of the boards or design materials constitutes an agreement to the :ref:`boards-terms-and-conditions`
    * Software images and purchase links available on the `board page <https://www.beagleboard.org/boards/beaglebone-ai>`__
    * For export, emissions and other compliance, see :ref:`bbai-support`
    * All support for BeagleBone AI design is through BeagleBoard.org community at `BeagleBoard.org forum <https://forum.beagleboard.org/tag/bbai>`_.

.. image:: images/BB_AI_handheld_500px.jpg
    :width: 740
    :align: center
    :alt: BeagleBone AI

.. only:: html

  .. grid:: 1 1 2 2
    :margin: 4 4 0 0
    :gutter: 4

    .. grid-item-card::
        :link: bbai-introduction
        :link-type: ref

        **1. Introduction**
        ^^^

        .. image:: images/chapter-thumbnails/01-introduction.*
            :align: center
            :alt: BeagleBone AI Chapter01 thumbnail
        
        +++

        Introduction to BeagleBone AI board with information on each component 
        location on both front and back of the board.

    .. grid-item-card:: 
        :link: bbai-quick-start
        :link-type: ref

        **2. Quick start**
        ^^^

        .. image:: images/chapter-thumbnails/02-quick-start.*
            :align: center
            :alt: BeagleBone AI Chapter02 thumbnail

        +++

        Getting started guide to enable you to start building your projects 
        in no time.

    .. grid-item-card:: 
        :link: bbai-design-and-specifications
        :link-type: ref

        **3. Design & Specifications**
        ^^^

        .. image:: images/chapter-thumbnails/03-design-and-specifications.*
            :align: center
            :alt: BeagleBone AI Chapter03 thumbnail

        +++

        Hardware and mechanical design and specifications of BeagleBone AI board 
        for those who want to know their board inside and out.

    .. grid-item-card:: 
        :link: bbai-expansion
        :link-type: ref

        **4. Expansion**
        ^^^

        .. image:: images/chapter-thumbnails/04-expansion.*
            :align: center
            :alt: BeagleBone AI Chapter04 thumbnail

        +++

        Connector pinout diagrams with expansion details so that you can 
        easily debug your connections and create custom expansion hardware.
        
    .. grid-item-card::
        :link: bbai-demos-and-tutorials
        :link-type: ref

        **5. Demos**
        ^^^

        .. image:: images/chapter-thumbnails/05-demos-and-tutorials.*
            :align: center
            :alt: BeagleBone AI Chapter5 thumbnail

        +++

        Demos and tutorials to quickly learn about BeagleBone AI capabilities.

    .. grid-item-card::
        :link: bbai-support
        :link-type: ref

        **6. Support**
        ^^^

        .. image:: images/chapter-thumbnails/06-support.*
            :align: center
            :alt: BeagleBone AI Chapter6 thumbnail

        +++

        Additional supporting information, images, documents, change history and
        hardware & software repositories including issue trackers.

.. toctree::
   :maxdepth: 1
   :hidden:

   01-introduction
   02-quick-start
   03-design-and-specifications
   04-expansion
   05-demos-and-tutorials
   06-support

